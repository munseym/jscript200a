$(document).ready(function () {
    const Flight = function (ident, aircrafttype, actualdeparturetime, estimatedarrivaltime,
        filed_departuretime, origin, destination, originName, originCity, destinationName,
        destinationCity) {
        this.ident = ident;
        this.aircrafttype = aircrafttype;;
        this.actualdeparturetime = actualdeparturetime;
        this.estimatedarrivaltime = estimatedarrivaltime;
        this.filed_departuretime = filed_departuretime;
        this.origin = origin;
        this.destination = destination;
        this.originName = originName;
        this.originCity = originCity;
        this.destinationName = destinationName;
        this.destinationCity = destinationCity;
    }

    Flight.prototype.getDisplayArrivalTime = function () {
        return new Date(this.estimatedarrivaltime * 1000).toLocaleString();
    }

    //Some flights returned are days old for some reason.  We don't want to display those.
    Flight.prototype.isAlreadyPast = function () {
        return this.estimatedarrivaltime < (Date.now() / 1000);
    }

    //Alert stuff
    const landingModal = document.getElementById('landingModal');
    const alertSetModal = document.getElementById('alertSetModal');
    const btn = document.getElementById("myBtn");
    const spans = Array.from(document.getElementsByClassName("close"));
    spans.forEach(function (span) {
        span.onclick = function () {
            landingModal.style.display = "none";
            alertSetModal.style.display = "none";
        }
    });

    window.onclick = function (event) {
        if (event.target == landingModal || event.target == alertSetModal) {
            landingModal.style.display = "none";
            alertSetModal.style.display = "none";
        }
    }

    // If an reminder button is clicked, start a timer for an alert
    const alertListener = function (e) {
        console.log($(this));
        const obj = $(this.children[3]);
        const estimatedarrivaltime = obj.get(0).innerText;
        console.log('Set an alert in ' + ((estimatedarrivaltime * 1000 - Date.now())/1000) + ' seconds.');
        alertSetModal.style.display = 'inline-block';

        setTimeout(function() {
            landingModal.style.display = 'inline-block';
          },
          estimatedarrivaltime * 1000 - Date.now()
        );
    };
    
    //Add a flight row to the web page
    const addFlightRow = function (flight) {
        const newItem = `<li><span>From ${flight.originCity}</span>
            <a class="move toLater">Arrives at ${flight.getDisplayArrivalTime()}</a>
            <a class="delete">Add reminder</a>
            <a class="estimatedarrivaltime">${flight.estimatedarrivaltime}</a>
            `;
        $myItem = $(newItem).click(alertListener);
        $('.today-list').append($myItem);
    }

    //This is a hack to get around CORS issues on localhost
    const flightScheduleUrl = `https://cors.io/?http://${API_KEY}@flightxml.flightaware.com/json/FlightXML2/Enroute?airport=KSEA`;
    console.log(flightScheduleUrl);
    fetch(flightScheduleUrl)
        .then(function (response) {
            return response.json();
        })
        .then(function (responseJson) {
            let flights = responseJson.EnrouteResult.enroute;
            let flt;
            flights.forEach(function (flight) {
                flt = new Flight(flight.ident, flight.aircrafttype, flight.actualdeparturetime, flight.estimatedarrivaltime,
                    flight.filed_departuretime, flight.origin, flight.destination, flight.originName, flight.originCity, flight.destinationName,
                    flight.destinationCity);
                localStorage.setItem(flight.ident, flight.estimatedarrivaltime);
                if (!flt.isAlreadyPast()) {
                    addFlightRow(flt);
                }
            });
        });
});