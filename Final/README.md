JSCRIPT200-final-project
========================

I created a website where one can set reminders for flights arriving at Seattle-Tacoma International Airport in the near future.

# Running the project
Create a file api-key.js that contains (provided separately)

const API_KEY='<Your FlightAware Key>';

# Implemented features
* One or more fetch requests to a 3rd party API)
* One or more timing functions
* Constructor function with a prototype
* Sets, updates, or changes cookies or local storage

# Screenshot

![screenshot](./Screenshot.png)