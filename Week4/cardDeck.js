/**
 * Returns an array of 52 Cards
 * @returns {Array} deck - a deck of cards
 */

const getVal = function(i){
    let v;
    if(i == 0){
        v = 11;
    }else if (i < 10){
        v = i + 1;
    }else{
        v = 10;
    }
    return v;
}

const getDisplayVal = function(i){
    let v;
    switch(i){
        case 0: 
            v = 'Ace';
            break;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
            v = (i + 1).toString();
            break;
        case 10:
            v = 'Jack';
            break;
        case 11:
            v = 'Queen';
            break;
        case 12:
            v = 'King';
            break;
    }
    return v;
}

const getUiVal = function(i){
    let v = getDisplayVal(i);
    if(v === 'Ace'){
        v = 'A';
    } else if(v === 'Jack'){
        v = 'K';
    } else if(v === 'Queen'){
        v = 'Q';
    }else if(v === 'King'){
        v = 'K';
    }
    return v;
}


const getDeck = function () {
    let deck = [];
    let suits = ['hearts', 'spades', 'clubs', 'diamonds'];
    for (let i = 0; i < 13; i++) {
        for (let suit in suits) {
            deck.push( {
                val: getVal(i),
                displayVal: getDisplayVal(i),
                uiVal: getUiVal(i),
                suit: suits[suit]
            });
        }
    }
    return deck;
}

const getRandomCard = function(){
    return deck[Math.floor(Math.random() * 52)];
}

const Deck = function(){
    let myDeck = getDeck();
    this.getRandomCard = function(){
        return myDeck[Math.floor(Math.random() * 52)];
    }
}

// CHECKS
const deck = getDeck();
console.log(`Deck length equals 52? ${deck.length === 52}`);

const randomCard = deck[Math.floor(Math.random() * 52)];

const cardHasVal = randomCard && randomCard.val && typeof randomCard.val === 'number';
console.log(`Random card has val? ${cardHasVal}`);

const cardHasSuit = randomCard && randomCard.suit && typeof randomCard.suit === 'string';
console.log(`Random card has suit? ${cardHasSuit}`);

const cardHasDisplayVal = randomCard &&
    randomCard.displayVal &&
    typeof randomCard.displayVal === 'string';
console.log(`Random card has display value? ${cardHasDisplayVal}`);