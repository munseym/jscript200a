const blackjackDeck = new Deck();

/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */
const CardPlayer = function(name) {
  let output = {
    name: name,
    hand: [],
    drawCard: function(){
      let card = blackjackDeck.getRandomCard();
      renderCard(this, card);
      output.hand.push(card);
      updateScore();
    }
  };
  return output;
};

// CREATE TWO NEW CardPlayers
let dealer = new CardPlayer('Dealer');
let player = new CardPlayer('Player');

/**
 * Using CSS to display a single card based on http://www.thatsoftwaredude.com/content/6196/coding-a-card-deck-in-javascript
 * The rest of the logic is my original.
 * 
 * Display a card
 * 
 * @param {CardPlayer} player 
 * @param {Ojbect} card 
 */
const renderCard = function(player, card){
  let playerdiv = document.getElementById(player.name);
  let carddiv = document.createElement("div");
  let icon;
  if (card.suit === 'hearts'){
	  icon='&hearts;';
  } else if (card.suit == 'spades'){
		icon = '&spades;';
  } else if (card.suit == 'diamonds'){
    icon = '&diams;';
  }	else{
    icon = '&clubs;';
  }
  carddiv.innerHTML = card.uiVal + '' + icon;
	carddiv.className = 'card';
	playerdiv.appendChild(carddiv);
}

/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} - Object containing Total points and whether hand isSoft
 */
const calcPoints = function(hand) {
  let output = {
    total: 0,
    isSoft: false
  }
  let aceCt = 0;
  hand.forEach(function(card){
    output.total += card.val;
    if(card.displayVal === 'Ace'){
      aceCt++;
    }
  });
  for(let i = 0; i < aceCt; i++){
    if(output.total > 21){
      output.total -= 10;
    }else{
      output.isSoft = true;
    }
  }
  return output;
}

/**
 * Determines whether the dealer should draw another card
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 */
const dealerShouldDraw = function(dealerHand) {
  let dealerScore = calcPoints(dealerHand);
  return (dealerScore.total < 17) || (dealerScore.total === 17 && dealerScore.isSoft);
}

/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore 
 * @param {number} dealerScore 
 * @returns {string} States the player's score, the dealer's score, and who wins
 */
const determineWinner = function(playerScore, dealerScore) {
  let winner;
  if(playerScore > 21){//Shouldn't happen
    winner = 'Dealer';
  }else if(dealerScore > 21){
    winner = 'Player';
  }else if(playerScore > dealerScore){
    winner = 'Player';
  }else if(dealerScore > playerScore){
    winner = 'Dealer';
  }else{
    winner = 'Tie';
  }
  return `Player: ${playerScore} Dealer ${dealerScore} Winner:${winner}`;
}

/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count 
 * @param {string} dealerCard 
 */
const getMessage = function(count, dealerCard) {
  return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`
}

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player 
 */
const showHand = function(player) {
  let displayHand = player.hand.map(function(card) { return card.displayVal});
  console.log(`${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);
}

const newgame = function(){
  showResult(' ');
  document.getElementById("standBtn").disabled = false;
  document.getElementById(dealer.name).innerHTML = "";
  document.getElementById(player.name).innerHTML = "";
  dealer.hand = [];
  player.hand = [];

  dealer.drawCard();
  player.drawCard();
  player.drawCard();
  showHand(player);
  let playerScore = calcPoints(player.hand).total;
  if(playerScore > 20){
    if(playerScore = 21){
      showResult('Blackjack!  You win!');
      document.getElementById("standBtn").disabled = true;
    }
    document.getElementById("hitBtn").disabled = true;
  }else{
    document.getElementById("hitBtn").disabled = false;
  }
}

const hit = function(){
  player.drawCard();
  let playerScore = calcPoints(player.hand).total;
  if(playerScore > 20){
    document.getElementById("hitBtn").disabled = true;
    if(playerScore > 21){
      showResult('You went over 21 - you lose!');
      document.getElementById("standBtn").disabled = true;
    }else if(playerScore == 21){
      stand();
    }
  }else{
    document.getElementById("hitBtn").disabled = false;
  }
}

const stand = function(){
  document.getElementById("standBtn").disabled = true;
  document.getElementById("hitBtn").disabled = true;
  do{
    dealer.drawCard();
    updateScore();
  }while(dealerShouldDraw(dealer.hand))
  showResult(determineWinner(calcPoints(player.hand).total, calcPoints(dealer.hand).total));
}

const updateScore = function(){
  document.getElementById('dealerScore').textContent = calcPoints(dealer.hand).total;
  document.getElementById('playerScore').textContent = calcPoints(player.hand).total;
}

const showResult = function(result){
  document.getElementById('result').textContent = result;
}

newgame();