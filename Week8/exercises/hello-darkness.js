let backGroundColor = 255;

// Change time remaining every 1 second
let countdownInterval = setInterval(function () {
    if (backGroundColor > 0) {
        backGroundColor--;
        document.body.style.backgroundColor = `rgb(${backGroundColor}, ${backGroundColor}, ${backGroundColor})`
    } else {
        clearInterval(countdownInterval);
    }
}, 500
);