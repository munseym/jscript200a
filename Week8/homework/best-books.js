// create api-key.js file with const API_KEY="your_api_key" in this same directory to use
const BASE_URL = 'https://api.nytimes.com/svc/books/v3/lists'
const URLEND = `hardcover-fiction.json?api-key=${API_KEY}`;

const url = `${BASE_URL}`;

const showBooks = function () {
    let year = document.getElementsByName('year')[0].value;
    let month = document.getElementsByName('month')[0].value;
    let date = document.getElementsByName('date')[0].value;
    let url = `${BASE_URL}/${year}-${month}-${date}/${URLEND}`;

    fetch(url)
        .then(function (response) {
            return response.json();
        })
        .then(function (responseJson) {
            let book;
            let bookHtml = '<table border="1"><tr><th>Title</th><th>Author</th><th>Description</th><th>Image</th></tr>';
            for (let i = 0; i < 5; i++) {
                book = responseJson.results.books[i];
                bookHtml += `<tr><td>${book.title}</td><td>${book.author}</td><td>${book.description}</td><td><img src="${book.book_image}"</td></tr>`
            }
            bookHtml += '</table>'
            document.getElementById('books-container').innerHTML = bookHtml;
        })
};

const formEl = document.getElementById('dateform')
    .addEventListener('submit', function (e) {
        showBooks();
        e.preventDefault();
    });