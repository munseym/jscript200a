let backgroundColor = 0;
const animate = function () {
    backgroundColor++;
    if (backgroundColor < 256) {
        document.body.style.backgroundColor = `rgb(${backgroundColor}, ${backgroundColor}, ${backgroundColor})`
        document.getElementsByTagName("h1")[0].style.color = `rgb(${255 - backgroundColor}, ${255 - backgroundColor}, ${255 - backgroundColor})`
        requestAnimationFrame(animate);
    }
}

requestAnimationFrame(animate);