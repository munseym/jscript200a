const calcPoints = function () {
    let playerHandEl = document.getElementById('player-hand');
    let cardEls = playerHandEl.getElementsByClassName('card');
    let hand = [];
    for (let i = 0; i < cardEls.length; i++) {
        hand.push({
            suit: cardEls[i].dataset.suit,
            val: parseInt(cardEls[i].dataset.val),
            displayVal: cardEls[i].dataset.displayVal,
        });
    }

    let hasAce = false;
    let handScore = 0;
    let isSoft = false;

    for (let i = 0; i < hand.length; i++) {
        const card = hand[i];

        if (card.displayVal === 'Ace') {
            hasAce = true;
            handScore += 1;
        } else {
            handScore += card.val;
        }
    }
    if (handScore <= 11 && hasAce) {
        handScore += 10;
        isSoft = true;
    }
    return {
        total: handScore,
        isSoft: isSoft
    }
}