describe("Blackjack", function () {

    it("Calculate 10, 7", function () {
        let hand = [
            {
                suit: 'hearts',
                val: 10,
                displayVal: '10'
            },
            {
                suit: 'spades',
                val: 7,
                displayVal: '7'
            }
        ];
        expect(determineScore(hand).total).toEqual(17);
    });
    it("Calculate Ace, 9", function () {
        let hand = [
            {
                suit: 'hearts',
                val: 11,
                displayVal: 'Ace'
            },
            {
                suit: 'spades',
                val: 9,
                displayVal: '9'
            }
        ];
        expect(determineScore(hand).total).toEqual(20);
    });
    it("Calculate 10, 7", function () {
        let hand = [
            {
                suit: 'hearts',
                val: 10,
                displayVal: '10'
            },
            {
                suit: 'spades',
                val: 6,
                displayVal: '6'
            },
            {
                suit: 'hearts',
                val: 11,
                displayVal: 'Ace'
            }
        ];
        expect(determineScore(hand).total).toEqual(17);
    });

});
