describe("dealerShouldDraw", function () {
    it("Calculate 10, 9", function () {
        let hand = [
            {
                suit: 'hearts',
                val: 10,
                displayVal: '10'
            },
            {
                suit: 'spades',
                val: 9,
                displayVal: '9'
            }
        ];
        expect(dealerShouldDraw(hand)).toBeFalsy();
    });
    it("Calculate Ace, 6", function () {
        let hand = [
            {
                suit: 'hearts',
                val: 11,
                displayVal: 'Ace'
            },
            {
                suit: 'spades',
                val: 6,
                displayVal: '6'
            }
        ];
        expect(dealerShouldDraw(hand)).toBeTruthy();
    });
    it("Calculate 10, 7", function () {
        let hand = [
            {
                suit: 'hearts',
                val: 10,
                displayVal: '10'
            },
            {
                suit: 'spades',
                val: 7,
                displayVal: '7'
            }
        ];
        expect(dealerShouldDraw(hand)).toBeFalsy();
    });
    it("Calculate 2, 4, 2, 5", function () {
        let hand = [
            {
                suit: 'hearts',
                val: 2,
                displayVal: '2'
            },
            {
                suit: 'spades',
                val: 4,
                displayVal: '4'
            }, {
                suit: 'hearts',
                val: 2,
                displayVal: '2'
            },
            {
                suit: 'spades',
                val: 5,
                displayVal: '5'
            }
        ];
        expect(dealerShouldDraw(hand)).toBeTruthy();
    });
});