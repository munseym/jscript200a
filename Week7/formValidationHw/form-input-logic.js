const formSelect = document.querySelector('select');
const jobsEls = document.getElementsByClassName('jobs');
const codingEls = document.getElementsByClassName('coding');

const removeClassFromCollection = function (collection, className) {
    for (let i = 0; i < collection.length; i++) {
        collection[i].classList.remove(className);
    }
}

const addClassToCollection = function (collection, className) {
    for (let i = 0; i < collection.length; i++) {
        collection[i].classList.add(className);
    }
}

formSelect.addEventListener('change', function (e) {
    if (this.value === 'jobs') {
        removeClassFromCollection(jobsEls, 'd-none');
        addClassToCollection(codingEls, 'd-none');
    } else {
        removeClassFromCollection(codingEls, 'd-none');
        addClassToCollection(jobsEls, 'd-none');
    }
});