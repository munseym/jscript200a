/**
 * Validates an individual input on form submit
 * @param {HTMLElement} inputEl 
 * @param {Event} submitEvent 
 */
const validateItem = function (inputEl, submitEvent) {
    let isValid = true;
    const errorEl = inputEl.parentElement.querySelector('.error');

    if (!inputEl.validity.valid) {
        const labelEl = inputEl.parentElement.querySelector('label');
        if (!inputEl.value) {
            errorEl.innerHTML = `${labelEl.innerText} is Required`;
        } else {
            errorEl.innerHTML = `${labelEl.innerText} is Not Valid`;
        }
        errorEl.classList.add('invalid');
        isValid = false;
    } else {
        const labelEl = inputEl.parentElement.querySelector('label');
        const emailRegex = /\w+@\w+\.\w+/;
        if (labelEl.innerText.endsWith('Name') && inputEl.value.length < 3) {
            errorEl.innerHTML = `${labelEl.innerText} must be at least 3 characters`;
            errorEl.classList.add('invalid');
            isValid = false;
        } else if (labelEl.innerText == 'Email' && !emailRegex.test(inputEl.value)) {
            errorEl.innerHTML = `Invalid email address`;
            errorEl.classList.add('invalid');
            isValid = false;
        } else if (labelEl.innerText === 'Message' && inputEl.value.length < 10) {
            errorEl.innerHTML = `${labelEl.innerText} must be at least 10 characters`;
            errorEl.classList.add('invalid');
            isValid = false;
        }
    }
    if (isValid) {
        errorEl.innerHTML = '';
        errorEl.classList.remove('invalid');
    }
    return isValid;
}

const inputElements = document.getElementsByClassName('validate-input');

const formEl = document.getElementById('connect-form')
    .addEventListener('submit', function (e) {
        const formSelect = document.querySelector('select');
        localStorage.setItem('contact_type',
            formSelect.value === 'coding' ? 'Talk Code' : 'Job opportunity');

        let isValid = true;
        for (let i = 0; i < inputElements.length; i++) {
            isValid = validateItem(inputElements[i], e) && isValid;
        }

        if (!isValid) {
            console.log('Bad input');

            // Prevent form submit
            e.preventDefault();
        }
    });
