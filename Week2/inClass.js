/**
 * CREATE AN OBJECT REPRESENTATION OF YOURSELF (10 MINS)
 * Should include:
 * - firstName
 * - lastName
 * - 'favorite food'
 * - mom & dad (objects with the same 3 properties as above)
 * 2. console.log dad's firstName, mom's favorite food
 */
let myself = {
    firstName: 'Michael',
    lastName: 'Munsey',
    'favorite food': 'pizza',
    mom: {
        firstName: 'Joyce',
        lastName: 'Munsey',
        'favorite food': 'ham'
    },
    dad: {
        firstName: 'Roger',
        lastName: 'Munsey',
        'favorite food': 'spaghetti'
    }
}
console.log(myself.dad.firstName);
console.log(myself.mom["favorite food"]);

/**
 * CREATE AN ARRAY TO REPRESENT THIS TIC-TAC-TOE BOARD (10 MINS)
 * You can use '-' for the empty squares.
 * After the array is created, O claims the top right square.
 * Update that value.
 * Log the grid to the console.
 * Hint: log each row separately.
 */
let ttt = [ ['-','-','-'],
            ['-','-','-'],
            ['-','-','-']];
ttt[0][2] = 'O';
for(var x = 0; x < 3; x++){
    console.log(ttt[x].join(' ')); 
}