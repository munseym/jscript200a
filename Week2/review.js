const assignmentDate = '1/21/2019';

// Convert to a Date instance
let dAssignmentDate = new Date(2019, 0, 21);


// Create dueDate which is 7 days after assignmentDate
let dueDate = dAssignmentDate;
dueDate.setDate(dueDate.getDate() + 7);
console.log(dueDate);

// Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
// log this value using console.log
let dtgParts = {
    "month": ('' + dueDate.getMonth() + 1).padStart(2, '0'),
    "day": ('' + dueDate.getDate()).padStart(2, '0'),
    "year": dueDate.getFullYear()
}
let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
let timeTag = '<time datetime="' + dtgParts.year + '-' + dtgParts.month + '-' + dtgParts.day + '">'
timeTag = timeTag + months[dueDate.getMonth()] + ' ' + dtgParts.day + ', ' + dtgParts.year + '</time>';
console.log(timeTag);