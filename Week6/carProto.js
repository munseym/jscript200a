/**
 * Creates a new Car object
 * @constructor
 * @param {String} model 
 */
const Car = function(model) {
    this.model = model;
    this.currentSpeed = 0;
}

Car.prototype.accelerate = function(){
    this.currentSpeed++;
};

Car.prototype.brake = function(){
    this.currentSpeed--;
};

Car.prototype.toString = function(){
    return `model: ${this.model} currentSpeed: ${this.currentSpeed}`;
};

let myCar = new Car('Prius');
console.log(myCar);
myCar.accelerate();
myCar.accelerate();
console.log(myCar);
myCar.brake();
console.log(myCar);
