/**
 * Creates a new instance of Pizza
 * @param {String} kind 
 */
const Pizza = function(kind) {
    this.kind  = kind;
    this.slices = 8;
}

Pizza.prototype.eatSlice = function(){
    this.slices--;
};

let pizza1 = new Pizza('Sausage');
let pizza2 = new Pizza('Ham');

pizza1.eatSlice();
console.log(pizza1.slices);

for(let i = 0; i < 5; i++){
    pizza2.eatSlice();
}
console.log(pizza2.slices);
