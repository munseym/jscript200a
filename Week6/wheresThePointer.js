// Attach one listener that will detect clicks on any of the <td>
// elements.  Should update that element's innerHTML to be the
// x, y coordinates of the mouse at the time of the click

let squares = document.getElementsByTagName("td");
for (let i = 0; i < squares.length; i++) {
    squares[i].addEventListener('click', function (e) {
        this.innerHTML = `${e.clientX}, ${e.clientY}`
    }, false);
}