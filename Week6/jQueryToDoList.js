$(document).ready(function () {
    // If an li element is clicked, toggle the class "done" on the <li>
    const doneListener = function (e) {
        $(this).toggleClass('done');
    };
    $('li').on('click', doneListener);

    // If a delete link is clicked, delete the li element / remove from the DOM
    const deleteListener = function (e) {
        //$(this).parent().remove();
        $(this).parent().fadeOut('slow');
    }
    $(".delete").on('click', deleteListener);

    // If a "Move to..."" link is clicked, it should move the item to the correct
    // list.  Should also update the working (i.e. from Move to Later to Move to Today)
    // and should update the class for that link.
    // Should *NOT* change the done class on the <li>
    const moveListener = function (e) {
        const isToday = ($(this).parent().parent().parent().hasClass('today'));
        const myUl = isToday ? '.later-list' : '.today-list';
        const moveToText = isToday ? 'Move to Today' : 'Move to Later';
        $(myUl).append($(this).parent());
        $(this).parent().toggleClass('done');
        $(this).text(`${moveToText}`);
        $(this).toggleClass('toLater');
        $(this).toggleClass('toToday');
    }
    $(".move").on('click', moveListener);

    // If an 'Add' link is clicked, adds the item as a new list item in correct list
    // addListItem function has been started to help you get going!  
    // Make sure to add an event listener to your new <li> (if needed)
    const addListItem = function (e) {
        e.preventDefault();
        const text = $(this).parent().find('input').val();
        const isToday = ($(this).parent().parent().hasClass('today'));
        const myUl = isToday ? '.today-list' : '.later-list';
        const moveToClass = isToday ? 'move toLater': 'move toToday';
        const moveToText = isToday ? 'Move to Later' : 'Move to Today';
        const newItem = `<li><span>${text}</span><a class="${moveToClass}">${moveToText}</a><a class="delete">Delete</a></li>`;

        $myItem = $(newItem).click(doneListener);
        jQuery($myItem).find('.delete').on('click', deleteListener);
        jQuery($myItem).find('.move').on('click', moveListener);

        $(myUl).append($myItem);
    }

    // Add this as a listener to the two Add links
    $('.add-item').on('click', addListItem);
});