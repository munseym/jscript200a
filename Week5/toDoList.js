const doneListener = function(e){
  if(e.target.tagName === 'LI'){
    if(e.target.className == 'done'){
      e.target.classList.remove('done');
    }else{
      e.target.className = 'done';
    }
  }else if(e.target.tagName === 'A'){
    if(e.target.classList.value === 'delete'){
      del(this, e);
    }else{
      moveTo(this, e);
    }
  }
}

// If an li element is clicked, toggle the class "done" on the <li>
for(let element of document.getElementsByTagName("li")){
  element.addEventListener('click', doneListener);
};

// If a delete link is clicked, delete the li element / remove from the DOM
const del = function(myThis, e){
  myThis.parentNode.removeChild(myThis);
}

// If a "Move to..."" link is clicked, it should move the item to the correct
// list.  Should also update the working (i.e. from Move to Later to Move to Today)
// and should update the class for that link.
// Should *NOT* change the done class on the <li>
const moveTo = function (myThis, e) {
  //console.log(e.target.parentNode.getElementsByTagName('span')[0]);
  if(myThis.parentNode.className == 'today-list'){
    // console.log(myThis);
    // console.log(e);
    document.getElementsByClassName('later-list')[0].appendChild(myThis);
    txt = document.createTextNode('Move to Today');
    myThis.childNodes[1].childNodes[0].remove();
    myThis.childNodes[1].appendChild(txt);
    myThis.childNodes[2].classList.replace('toLater', 'toToday');
  }else{
    document.getElementsByClassName('today-list')[0].appendChild(myThis);
    myThis.childNodes[1].childNodes[0].remove();
    txt = document.createTextNode('Move to Later');
    myThis.childNodes[1].appendChild(txt);
    myThis.childNodes[2].classList.replace('toToday', 'toLater');
  }
}


// If an 'Add' link is clicked, adds the item as a new list item in correct list
// addListItem function has been started to help you get going!  
// Make sure to add an event listener to your new <li> (if needed)
const addListItem = function (e) {
  e.preventDefault();
  const input = this.parentNode.getElementsByTagName('input')[0];
  input.parentNode.parentNode.className
  const text = input.value; // use this text to create a new <li>
  const ul = input.parentNode.parentNode.childNodes[3];
  const li = document.createElement('li');
  const span = document.createElement('span');
  span.appendChild(document.createTextNode(text));
  li.appendChild(span);
  
  const mv = document.createElement('a');
  if(input.parentNode.parentNode.className === 'today'){
    mv.appendChild(document.createTextNode('Move to Later'));
    mv.classList.add('move');
    mv.classList.add('toLater');
  }else{
    mv.appendChild(document.createTextNode('Move to Today'));
    mv.classList.add('move');
    mv.classList.add('toToday');

  }
  li.appendChild(mv);

  const dl = document.createElement('a');
  dl.appendChild(document.createTextNode('Delete'));
  dl.classList.add('delete');
  li.appendChild(dl);
  li.addEventListener('click', doneListener);

  ul.appendChild(li);
}

// Add this as a listener to the two Add links
for(let element of document.getElementsByClassName("add-item")){
  element.addEventListener('click', addListItem);
};