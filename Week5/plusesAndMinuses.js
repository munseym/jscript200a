let counter = 0;

const plusEl = document.getElementById('plus');
plusEl.addEventListener('click', function(e){
    counter++;
    document.getElementById('count').textContent = counter;
});

const minusEl = document.getElementById('minus');
minusEl.addEventListener('click', function(e){
    counter--;
    document.getElementById('count').textContent = counter;
});