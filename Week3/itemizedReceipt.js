// create function logReceipt that accepts menu items (1 or many) as objects
// with these properties: {descr, price}
// i.e. {descr: 'Coke', price: 1.99}
// function should log each item to the console and log a total price
const logReceipt = function(){
  let args = Array.from(arguments);
  let total = 0;
  let item = '';
  let taxRate = args.shift();
  while(args.length > 0){
     item = args.shift();
     total += item.price;
     console.log(`${item.descr} - \$${item.price.toFixed(2)}`);
  }
  console.log(`Subtotal - \$${total.toFixed(2)}`);
  console.log(`Tax - \$${(total * taxRate).toFixed(2)}`);
  console.log(`Total - \$${(total * (1 + taxRate)).toFixed(2)}`);
};

// Check
logReceipt(
    0.1,
    {descr: 'Burrito', price: 5.99},
    {descr: 'Chips & Salsa', price: 2.99},
    {descr: 'Sprite', price: 1.99}
  );
  // should log something like:
  // Burrito - $5.99
  // Chips & Salsa - $2.99
  // Sprite - $1.99
  // Total - $10.97
 
//Extra Credit
console.log();
const bud = {descr: 'Bud Light', price: 3.99};
const burger = {descr: 'Hamburger', price: 6.99};
logReceipt(0.1, bud, burger);