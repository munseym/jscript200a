const regex1 = RegExp('\\(\\d{3}\\)\\s\\d{3}-\\d{4}');
const regex2 = RegExp('\\d{3}-\\d{3}-\\d{4}');
const regex3 = RegExp('\\d{3}\\s\\d{3}\\s\\d{4}');

const regex4 = RegExp('(\\d{3,4})', 'g');

const validator = function (phone) {
    if (regex1.test(phone)) {
        return true;
    } else if (regex2.test(phone)) {
        return true;
    } else if (regex3.test(phone)) {
        return true;
    } else {
        return false;
    }
}


const parsePhoneNumber = function (phone) {
    if(validator(phone) === true){
        let match;
        let result = [];
        while(match = regex4.exec(phone)){
            result.push(match[0]);
        }
        let areaCode = result.shift();
        let lastSeven = result.join('');
        return {
            areaCode: areaCode,
            phoneNumber: lastSeven
        }
    }else{
        return {
            areaCode: 'Bad input',
            phoneNumber: 'Bad input'
        }
    }
};

console.log(validator('(206) 333-4444'));
console.log(validator('206-333-4444'));
console.log(validator('206 333 4444'));
console.log(validator('(206) 33-4444'));

console.log(parsePhoneNumber('(206) 333-4444'));
console.log(parsePhoneNumber('206-333-4444'));
console.log(parsePhoneNumber('206 333 4444'));

console.log(parsePhoneNumber('(20) 333-4444'));
