const RESULT_VALUES = {
    w: 3,
    d: 1,
    l: 0
}

// This function accepts one argument, the result, which should be a string
// Acceptable values are 'w', 'l', or 'd'
const getPointsFromResult = function getPointsFromResult(result) {
    return RESULT_VALUES[result];
}

// This function accepts two arguments
//   The result, which should be a string : Acceptable values are 'w', 'l', or 'd'
//   A string of results including wins, draws, and losses i.e. 'wwdlw'
// Returns the total points earned by a 'w', 'l', or 'd'
const getPointsFromResultString = function(character, string){
    let regex = RegExp('(' + character + ')', 'g');
    return (string.match(regex) || []).length * getPointsFromResult(character);
}

// Create getTotalPoints function which accepts a string of results
// including wins, draws, and losses i.e. 'wwdlw'
// Returns total number of points won
const getTotalPoints = function (wlstring) {
    return  getPointsFromResultString('w', wlstring) + 
            getPointsFromResultString('d', wlstring) + 
            getPointsFromResultString('l', wlstring);
};

// Check getTotalPoints
console.log(getTotalPoints('wwdl')); // should equal 7

// create orderTeams function that accepts as many team objects as desired, 
// each argument is a team object in the format { name, results }
// i.e. {name: 'Sounders', results: 'wwlwdd'}
// Logs each entry to the console as "Team name: points"

const orderTeams = function () {
    let teams = Array.from(arguments);
    //another alternative would be to calculate getTotalPoints once and store it in the object
    teams.sort(function(a,b){
        return getTotalPoints(b.results) - getTotalPoints(a.results)
    });
    teams.forEach(function(team){
        console.log(`${team.name}: ${getTotalPoints(team.results)}`);
    });
}

// Check orderTeams
orderTeams(
    { name: 'Sounders', results: 'wwdl' },
    { name: 'Galaxy', results: 'wlld' }
);
  // should log the following to the console:
  // Sounders: 7
  // Galaxy: 4