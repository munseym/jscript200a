// Create a constructor function SpaceShip
// - should set two properties: name and topSpeed
// - should have a method accelerate that logs to the console 
//   `${name} moving to ${topSpeed}`
const SpaceShip = function SpaceShip(name, topSpeed) {
    this.name = name;
    this.topSpeed = topSpeed;
    this.accelerate = function () {
        console.log(`${name} moving to ${topSpeed}`);
    }
}


// Make name and topSpeed private
const SpaceShip2 = function SpaceShip(inName, inTopSpeed) {
    let name = inName;
    let topSpeed = inTopSpeed;
    this.accelerate = function () {
        console.log(`${name} moving to ${topSpeed}`);
    }
};

// Keep both name and topSpeed private, but 
// add a method that changes the topSpeed
const SpaceShip3 = function SpaceShip(inName, inTopSpeed) {
    let name = inName;
    let topSpeed = inTopSpeed;
    this.accelerate = function () {
        console.log(`${name} moving to ${topSpeed}`);
    }
    this.changeTopSpeed = function (newTopSpeed) { 
        topSpeed = newTopSpeed;
        console.log(`Changed top speed to ${topSpeed}`);
    }
};

// Call the constructor with a couple ships, change the topSpeed
// using the method, and call accelerate.
console.log('Part 1');
const s1 = new SpaceShip('Michael1', 10);
console.log(s1.name);
s1.accelerate();

console.log('\nPart 2');
const s2 = new SpaceShip2('Michael2', 10);
console.log(s2.name);
console.log(s2.topSpeed);
s2.accelerate();

console.log('\nPart 3');
const s3 = new SpaceShip3('Michael3', 10);
s3.accelerate();
s3.changeTopSpeed('20');
s3.accelerate();
console.log(s3.topSpeed);
console.log(s3.name);