/*
 * Validate the email
 * You are given an email as string myEmail
 * make sure it is in correct email format.
 * Should be in this format, no whitespace:
 * 1 or more characters
 * @ sign
 * 1 or more characters
 * .
 * 1 or more characters
 */
const myEmail = 'foo@bar.baz'; // good email
const badEmail = 'badmail@gmail'; // bad email

const emailValidator = RegExp('^\\w+@\\w+\\.\\w+');

console.log(emailValidator.test(myEmail));
console.log(emailValidator.test(badEmail));